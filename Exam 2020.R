setwd("C:/Users/??????????/Desktop/?????????? ??????")
assurance.original <- read.csv("assurance.csv")
assurance <-assurance.original


str(assurance)
summary(assurance)
head(assurance)

install.packages("lubridate")
library(lubridate)
#create a new column with the difference between CallStart and CallEnd
assurance$call<-difftime(as.POSIXct(assurance$CallEnd, format = "%H:%M:%S"), as.POSIXct(assurance$CallStart, format = "%H:%M:%S"))

#create a new dataframe with ten parameters
cars.assurance <- assurance
cars.assurance$Education <-NULL                                  
cars.assurance$CallEnd <-NULL
cars.assurance$CallStart  <-NULL
cars.assurance$Outcome  <-NULL
cars.assurance$Communication  <-NULL
cars.assurance$LastContactDay  <-NULL
cars.assurance$LastContactMonth <-NULL
cars.assurance$DaysPassed <-NULL
cars.assurance$NoOfContacts  <-NULL
cars.assurance$id <-NULL

str(cars.assurance)
summary(cars.assurance)
head(cars.assurance)

#change numeric to factor if releventi
cars.assurance$Default <- as.factor(cars.assurance$Default)
cars.assurance$CarInsurance <- as.factor(cars.assurance$CarInsurance)
cars.assurance$CarLoan<- as.factor(cars.assurance$CarLoan)
cars.assurance$HHInsurance<- as.factor(cars.assurance$HHInsurance)


any(is.na(cars.assurance))
any(is.na(cars.assurance$CarInsurance))
summary(cars.assurance)


table(cars.assurance$Job)
cars.assurance$Job<- as.factor(cars.assurance$Job)

#replace the NA
replace <-function(x){
  if (is.na (x)) return ('unemployed')
  return (x)
}

cars.assurance$Job<- sapply(cars.assurance$Job, replace)
cars.assurance$Job<- as.factor(cars.assurance$Job)



library(caTools)
#trainning test and test set
filter <- sample.split(cars.assurance$CarInsurance, SplitRatio = 0.7)
cars.assurance.train <- subset( cars.assurance, filter == T)
cars.assurance.test <- subset(cars.assurance, filter == F)

library(rpart)
library(rpart.plot)

#decision tree 
model.dt <- rpart(CarInsurance ~ ., method = 'class', data = cars.assurance.train)
rpart.plot(model.dt, box.palette="RdBu", shadow.col="gray", nn=TRUE)
prp(model.dt)
predictDt<- predict(model.dt, cars.assurance.test)

#install.packages("randomForest")
library("randomForest")
any(is.na(cars.assurance))
sum(is.na(cars.assurance.train))

summary(cars.assurance)

#random tree
model.rf <- randomForest(CarInsurance ~ ., data = cars.assurance.train, importance = TRUE)

prediction.rf <- predict(model.rf,cars.assurance.test)
actual <- cars.assurance.test$CarInsurance
prediction.rf<- predict(model.rf,cars.assurance.test, type = 'response')
predicted.values <- ifelse(prediction.rf>0.6,1,0)
confusion_matrix <- table(cars.assurance.test$CarInsurance)
recall <- confusion_matrix[1,1] /(confusion_matrix[1,1] + confusion_matrix[1,2])
precision <- confusion_matrix[1,1] /(confusion_matrix[1,1] + confusion_matrix[2,1])

library(pROC)

rocCurveDT <- roc(cars.assurance.test$CarInsurance, predictDt[,"0"], direction = ">", levels = c("0","1"))
rocCurveRF <- roc(cars.assurance.test$CarInsurance, as.numeric(prediction.rf), direction = "<", levels = c("0","1"))

plot(rocCurveDT, col="red", main='ROC chart')
par(new=TRUE)
plot(rocCurveRF, col="blue", main='ROC chart')

